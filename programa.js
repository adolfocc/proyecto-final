x=[856.35,854.29,854.22,854.21]
function ordenar(x){
    x=x.sort()
    return x
}

function mediana(x){
    let a,c
    ordenar(x)
    a=x.length/2  
    if(Number.isInteger(a)){
        c=(x[a]+x[a-1])/2
    }else{
        c=x[Math.floor(a)]
    }
    return c
}
function medicionesAceptadas(x){
    x=ordenar(x)
    let a,i,c,b,d=0
    a=1.3
    b=mediana(x)
    
    for(i=0;i<x.length;i++){
        c=Math.abs(x[i]-b)
        if(c<a){
            d=d+1
        }
    }
    return d
}

function valorProbable(x){
    x=ordenar(x)
    let a,i,c,b,d=0,e,f
    a=1.3
    b=mediana(x)
    e=medicionesAceptadas(x)
    for(i=0;i<x.length;i++){
        c=Math.abs(x[i]-b)
        if(c<a){
            d=d+x[i]
        }
    }
    f=d/e
    return Number(f.toFixed(3))
}
function errorProbable(x){
    x=ordenar(x)
    let a,b,c,d=0,e,f,g,h,i
    a=1.3
    b=mediana(x)
    e=medicionesAceptadas(x)
    f=valorProbable(x)
    for(i=0;i<x.length;i++){
        c=Math.abs(x[i]-b)
        g=Math.abs(f-x[i])
        if(c<a){
            d=d+Math.pow(g,2)
        }
    }
    h=0.6745*(Math.pow((d/(e-1)),0.5))
    return h
}

function validez(x){
    let a,b
    a=medicionesAceptadas(x)
    b=errorProbable(x)
    if(a>=3 && b<0.2){
        return "Valor Probable del lindero es válido"
    }else{
        return "Valor Probable del lindero no es válido"
    }
}

